// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import JustWatch from "../../JustWatch.js";

export default (req, mainRes) => {
    const body = JSON.parse(req.body)
    const justWatch = new JustWatch({'locale':body.lang});
    let providers;

    justWatch.getProviders().then((res) => {
        providers = res;
        mainRes.status(200).json({ data: res})
    })
    // mainRes.status(200).json({ data: 'tes'})
}
