// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import JustWatch from "../../JustWatch.js";

export default (req, mainRes) => {
  const justWatch = new JustWatch();
  let providers;

  justWatch.getProviders().then((res) => {
    providers = res;
    mainRes.status(200).json({ name: res})
  }).then(() => {
  });

}
