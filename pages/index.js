import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

import {useEffect, useRef, useState} from "react";

import LanguagePicker from "../components/language_picker";
import {languages} from "../languages";
import MovieList from "../components/movie_list";
import fetch from "node-fetch";
import * as ga from "../lib/ga";

export default function Home() {
    const [query, setQuery] = useState("");
    const [movieList, setSetMovies] = useState([{id: ''}]);
    const [providers, setProviders] = useState();
    const [loading, setLoading] = useState(false);

    const [language, setLanguage] = useState({
        id: 1,
        name: 'United States',
        code: 'en-US',
        flag: 'us.svg',
    });

    const isInitialMount = useRef(true);

    useEffect(() => {
        if (isInitialMount.current) {
            const browserLanguage = navigator.language || navigator.userLanguage;
            const language = languages.find((language) => {
                return language.code === browserLanguage;
            });
            setLanguage(language);
            isInitialMount.current = false;
        }
    })

    const getProvider = (providerId) => {
        const provider = providers.find((provider) => {
            return providerId === provider.id
        })
        return provider;
    };


    const search = (event) => {
        event.preventDefault();


        ga.event({
            action: "search",
            params: {
                search_term: query
            }
        })

        setLoading(true);
        fetch('/api/providers', {
            method: "POST",
            body: JSON.stringify({
                lang: language.code.replace('-', '_'),
            })
        })
            .then(response => response.json())
            .then(res => {
                setProviders(res.data)
            }).then(() => {
                if (query.length) {
                    fetch(`api/search`, {
                        method: "POST",
                        body: JSON.stringify({
                            lang: language.code.replace('-', '_'),
                            query: query,
                        })
                    })
                        .then(response => response.json()).then(res => {
                        console.log(res.data.items)
                        setSetMovies(res.data.items);
                        setLoading(false);
                    })
                }
            }
        );


    }
    return (
        <div>
            <Head>
                <title>The Platform</title>
                <meta name="description"
                      content="You can search your favorite movie / tv series on all streaming services"/>
                <link rel="icon" href="/favicon.ico/favicon.ico"/>
                <link rel="preconnect" href="https://fonts.gstatic.com"/>
                <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@300;400;500;700&display=swap"
                      rel="stylesheet"/>

                <meta charSet='utf-8'/>
                <meta http-equiv='X-UA-Compatible' content='IE=edge'/>
                <meta name='viewport'
                      content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no'/>
                <meta name='description' content='Description'/>
                <meta name='keywords' content='Keywords'/>

                <link rel="manifest" href="/manifest.json"/>
                <link href='/favicon.ico/favicon-16x16.png' rel='icon' type='image/png' sizes='16x16'/>
                <link href='/favicon.ico/favicon-32x32.png' rel='icon' type='image/png' sizes='32x32'/>
                <link rel="apple-touch-icon" href="/favicon.ico/apple-icon.png"></link>
                <meta name="theme-color" content="#317EFB"/>
                <script
                    async
                    src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}
                />
                <script
                    dangerouslySetInnerHTML={{
                        __html: `
                            window.dataLayer = window.dataLayer || [];
                            function gtag(){dataLayer.push(arguments);}
                            gtag('js', new Date());
                            gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}', {
                              page_path: window.location.pathname,
                            });
                          `,
                    }}
                />
            </Head>
            <main>
                <div className={'main-wrapper'}>
                    <div className={styles.fixedBg}>
                    </div>
                    <div className="grid grid-col-12">
                        <div className={styles.languagePickerContainer}>
                            <LanguagePicker setLanguageProp={setLanguage} selectedLanguage={language}/>
                        </div>
                        <div className="col-span-12 text-center mt-24">
                            <h1 className={styles.mainTitle}>The Platform</h1>
                        </div>
                        <form onSubmit={search} className={!loading ? 'col-span-12 grid grid-cols-12' : 'col-span-12 grid grid-cols-12 opacity-20'}>
                            <div className="col-span-12 relative">
                                <button>
                                    <svg xmlns="http://www.w3.org/2000/svg" width={'25px'}
                                         className='searchIcon'
                                         style={{position: 'absolute', bottom: '13', right: '15', color: 'white'}}
                                         fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                    </svg>
                                </button>
                                <input
                                    autoFocus={true}
                                    autoComplete="off"
                                    type="text"
                                    name="email"
                                    id="email"
                                    className={styles.searchInput}
                                    placeholder="Search Movie / Tv Show"
                                    onChange={e => setQuery(e.target.value)}
                                />
                            </div>
                            {/*<div className="col-span2">*/}
                            {/*    <button>Search</button>*/}
                            {/*</div>*/}
                        </form>
                    </div>
                    {loading &&
                    <div className={styles.loadingContainer}>
                        <img src="./loading.gif" alt="Loading Gif"/>
                    </div>
                    }
                    {movieList.length !== 1 && !loading &&
                    <div className={styles.listContainer}>
                        <MovieList getProviderProp={getProvider} moviesProp={movieList}></MovieList>
                    </div>
                    }

                </div>

            </main>
        </div>
    )
}

// export async function getStaticProps(context) {
//     const res = await fetch(`http://localhost:3000/api/providers`)
//     const data = await res.json()
//
//     if (!data) {
//         return {
//             notFound: true,
//         }
//     }
//
//     return {
//         props: {}, // will be passed to the page component as props
//     }
// }