import React from "react";

export default function ListItem(props) {
    const movie = props.movieProp;
    const offers = movie.offers ?? [];


    const getProvider = props.getProviderProp;

    const filteredOffers = offers.filter((offer) => {
        console.log(offer);
        return offer.monetization_type === 'flatrate' && offer.presentation_type === 'sd';
    })


    const fetchPosterCode = (fullPath = '') => {
        const posterCode = fullPath.split('/')
        return posterCode[2];
    }

    return (movie.poster ? (
            <div className="grid grid-cols-12 gap-6 items-center lg:mb-10 mb-0">
                <div className={"col-span-2 lg:col-span-1"}>
                    <img className="w-100"
                         src={`https://images.justwatch.com/poster/${fetchPosterCode(movie.poster)}/s592/poster.webp`}
                         alt={movie.title}/>
                </div>
                <div className={"col-span-10 lg:col-span-7  align-self-center"}>
                    <p className="text-sm font-medium text-gray-900">{movie.title}</p>
                </div>
                {filteredOffers.length ?
                    <div
                        className={"col-start-3 lg:col-start-0 col-span-12 logoWrapper lg:col-span-4 align-middle text-left lg:text-right"}>
                        <>
                            {filteredOffers.map((platform, index) => {
                                console.log(platform,movie.title);
                                const provider = getProvider(platform.provider_id) ?? {provider: {icon_url: ''}};

                                return (
                                    <a href={platform.urls.standard_web} key={index}
                                       className={'inline-block ml-0 lg:ml-3'}
                                       target={'_blank'}>
                                        {provider.icon_url &&
                                        <div>

                                            <img
                                                width={'45px'}
                                                src={`https://images.justwatch.com/icon/${fetchPosterCode(provider.icon_url)}/s100`}
                                                alt=""/>
                                        </div>

                                        }

                                    </a>
                                )
                            })}
                        </>
                    </div> : ''
                }

            </div>
        ) : ''
    )
}