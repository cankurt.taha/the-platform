/* This example requires Tailwind CSS v2.0+ */
import {Fragment, useEffect, useRef, useState} from 'react'
import {Listbox, Transition} from '@headlessui/react'
import {CheckIcon, SelectorIcon} from '@heroicons/react/solid'
import JustWatch from "justwatch-api";
import {languages} from "../languages";

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function LanguagePicker(props) {

    useEffect(() => {
        const language = languages.find((language) => {
            return language.code === props.selectedLanguage.code;
        });
    })

    return (
        <Listbox value={props.selectedLanguage} onChange={props.setLanguageProp}>
            {({open}) => (
                <>
                    <div className="mt-1 relative" style={{minWidth: '200px'}}>
                        <Listbox.Button
                            className="relative w-full text-white rounded-md shadow-sm pl-3 pr-10 py-2   sm:text-sm focus:outline-none ring-none">
              <span className="flex items-center">
                <img src={props.selectedLanguage.flag} alt="" className="flex-shrink-0 h-6 w-6 rounded-full"/>
                <span className="ml-3 block truncate">{props.selectedLanguage.name}</span>
              </span>
                            <span
                                className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <SelectorIcon className="h-5 w-5 text-gray-400" aria-hidden="true"/>
              </span>
                        </Listbox.Button>

                        <Transition
                            show={open}
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Listbox.Options
                                static
                                className="lang-picker-wrapper absolute z-10 mt-1 w-full text-white color-white shadow-lg max-h-56 rounded-md py-1  ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
                            >
                                {languages.map((person) => (
                                    <Listbox.Option
                                        key={person.id}
                                        className={({active}) =>
                                            classNames(
                                                active ? 'text-white bg-gray-600 cursor-pointer' : 'text-gray-900',
                                                'cursor-default select-none relative py-2 pl-3 pr-9'
                                            )
                                        }
                                        value={person}
                                    >
                                        {({selected, active}) => (
                                            <>
                                                <div className="flex items-center text-white">
                                                    <img src={person.flag} alt=""
                                                         className="flex-shrink-0 h-6 w-6 rounded-full"/>
                                                    <span
                                                        className={classNames(selected ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}
                                                    >
                            {person.name}
                          </span>
                                                </div>

                                            </>
                                        )}
                                    </Listbox.Option>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </>
            )}
        </Listbox>
    )
}