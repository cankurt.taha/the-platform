import React from "react";
import ListItem from "./list_item";
import styles from '../styles/List.module.css'

export default function MovieList(props) {
    const getProvider = props.getProviderProp;
    return (
        <>
            {props.moviesProp.map((movie) => {
                return (
                    <div key={movie.id} className={'listItem'}>
                        <ListItem getProviderProp={getProvider} movieProp={movie}/>
                    </div>
                )
            })}
        </>
    )
}