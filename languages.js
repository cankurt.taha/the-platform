export const languages = [
    {
        id: 1,
        name: 'Germany',
        code: 'de-DE',
        flag: 'de.svg',
    },

    {
        id: 2,
        name: 'France',
        code: 'fr-FR',
        flag: 'fr.svg'
    },
    {
        id: 3,
        name: 'United States',
        code: 'en-US',
        flag: 'us.svg',
    },
    {
        id: 4,
        name: 'Turkey',
        code: 'tr-TR',
        flag: 'tr.svg',
    }
]
